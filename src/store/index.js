import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    eyeClosed: false
  },
  mutations: {
    setEyeClosed (state) {
      state.eyeClosed = !state.eyeClosed
    }
  },
  getters: {
    isEyeClosed (state) {
      return state.eyeClosed
    }
  },
  actions: {
    // eyeAction (store) {
    //   store.state.eyeClosed
    //   store.getters.isEyeClosed
    //   store.commit('setEyeClosed')
    // }
  }
})
