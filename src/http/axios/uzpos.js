import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { Message } from 'element-ui'
import config from '../../config/main.json'

Vue.use(VueAxios, axios)

const instance = axios.create({
  baseURL: config.API_URL_UZPOS,
  timeout: 5000
})

instance.interceptors.response.use(
  function (res) {
    if (res.status === 200) {
      return res.data
    } else {
      Message({
        showClose: true,
        message: 'Status 200 emas',
        type: 'error',
        duration: 5000
      })
      return null
    }
  },
  function (error) {
    Message({
      showClose: true,
      message: 'Xatolik yuz berdi',
      type: 'error',
      duration: 5000
    })
    return Promise.reject(error)
  }
)

export default instance
