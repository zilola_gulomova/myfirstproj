import request from '../axios/jsonPlaceholder';

// export function postsList () {
//   return request({
//     method: 'get',
//     url: 'posts'
//   })
// }

export function postsList () {
  return request.get('posts')
}

export function postsUpdate (data) {
  return request.put('posts/1', data)
}
