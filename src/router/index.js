import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

const Portfolio = require('../views/Portfolio.vue').default
Vue.use(VueRouter)

const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/about', name: 'About', component: () => import('../views/About.vue') },
  { path: '/portfolio', name: 'Portfolio', component: Portfolio }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
